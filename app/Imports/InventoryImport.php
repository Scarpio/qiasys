<?php


namespace App\Imports;


use App\Models\InventoryModel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Ramsey\Uuid\Uuid;

class InventoryImport implements ToModel, WithHeadingRow
{
    public function model(array $row) {
        return new InventoryModel([
            'consignee' => $row['consignee'],
            'shipper' => $row['shipper'],
            'purchase_order' => $row['purchase_order'],
            'carrier' => $row['carrier'],
            'airway_bill' => $row['airway_bill'],
            'quantity' => $row['quantity'],
            'eta_time' => $row['eta_time'],
            'arrival_time' => $row['arrival_time'],
            'broker' => $row['broker'],
            'driver' => $row['driver'],
            'origin' => $row['origin'],
            'description' => $row['description'],
            'net_weight_lbs' => $row['net_weight_lbs'],
        ]);
    }
}
