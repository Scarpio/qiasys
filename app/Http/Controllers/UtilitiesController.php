<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UtilitiesController extends Controller
{
    public function validateUrl(Request $request, $domain) {
        $domainExemptions = ['bellsouth.net', 'sbcglobal.net'];
        if(in_array(strtolower($domain), $domainExemptions)) {
            return 'valid';
        }
        if($socket =@ fsockopen($domain, 80, $errno, $errstr, 30)) {
            fclose($socket);
            return 'valid';
        }
        \Illuminate\Support\Facades\Log::info('Domain name validation failed to pass', array($domain));
        return 'invalid';
    }
}
