<?php

namespace App\Http\Controllers;

use App\Models\InventoryModel;
use App\Models\OrderCartModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class OrderController extends Controller {
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function addToOrderForm(Request $request, $id) {
        try {
            $inventoryItem = \Illuminate\Support\Facades\DB::table('inventory')
                ->where('id', $id)
                ->where('account_id', \auth()->user()->account_id)
                ->first();
            $title = 'Add Inventory Item to Order';
            return view('order.add-to-order-frm')
                ->with('title', $title)
                ->with('inventoryItem', $inventoryItem);
        } catch (\Exception $e) {
            $title = 'Add Inventory Item to Order';
            $message = 'There was a problem getting the inventory item. ' . $e->getMessage();
            $class = 'alert-danger';
            return view('message-view')
                ->with('title', $title)
                ->with('message', $message)
                ->with('class', $class);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function processAddToOrderForm(Request $request) {
        $title = 'Add Inventory Item to Order';
        try {
            $order = new OrderCartModel();
            $order->user_id = \auth()->user()->id;
            $order->account_id = \auth()->user()->account_id;
            $order->inventory_id = $request->get('id');
            if ($request->get('quantityAvailable') > $request->get('quantityToOrder') &&
                $request->get('quantityToOrder') > 0) {
                $order->quantity = $request->get('quantityToOrder');
            } else {
                $order->quantity = 0;
            }
            // get the current cart id, if there is none assign one.
            if ($currentCart = OrderCartModel::query()->where('user_id', \auth()->user()->id)->first()) {
                $order->cart_id = $currentCart['cart_id'];
            } else {
                $order->cart_id = Uuid::uuid4();
            }
            $order->save();
            // Now we need to deplete the inventory so it does not show as available anymore.
            $inventory = InventoryModel::find($request->get('id'));
            $inventory->quantity = $inventory->quantity - $request->get('quantityToOrder');
            $inventory->save();
            $message = 'Success, the inventory item has been added to the cart.';
            $class = 'alert-success';
            return view('message-view')
                ->with('title', $title)
                ->with('message', $message)
                ->with('class', $class);
        } catch(\Exception $e) {
            $message = 'There was a problem adding the inventory item to the cart. ' . $e->getMessage();
            $class = 'alert-danger';
            return view('message-view')
                ->with('title', $title)
                ->with('message', $message)
                ->with('class', $class);
        }
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showOrders() {
        $title = 'View Order Cart';
        try {
            $cartOrderItems = DB::table('order_cart')
                ->join('inventory', 'inventory.id', '=', 'order_cart.inventory_id')
                ->where('order_cart.user_id', \auth()->user()->id)
                ->where('order_cart.account_id', \auth()->user()->account_id)
                ->get();
            $columns = \Illuminate\Support\Facades\Schema::getColumnListing('order_cart');
            return view('order.list')
                ->with('columns', $columns)
                ->with('title', $title)
                ->with('cartOrderItems', $cartOrderItems);
        } catch(\Exception $e) {
            $message = 'There was a problem getting your order cart. ' . $e->getMessage();
            $class = 'alert-danger';
            return view('message-view')
                ->with('title', $title)
                ->with('message', $message)
                ->with('class', $class);
        }

    }
}
