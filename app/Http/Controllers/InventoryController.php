<?php

namespace App\Http\Controllers;


use App\Imports\InventoryImport;
use App\Models\InventoryModel;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Excel;
use Illuminate\Support\Facades\Auth;

class InventoryController extends Controller {
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function displayUploadForm() {
        return view('inventory.upload-file');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function loadInventoryData(Request $request) {
        try {
            $this->validate($request, [
                'excel_file' => 'required|mimes:xls,xlsx'
            ]);
            $request->file('excel_file')->getRealPath();
            Excel::import(new InventoryImport, $request->file('excel_file'));
            return view('message-view')
                ->with('message', 'The upload was successful')
                ->with('title', 'The inventory file Upload was successful')
                ->with('class', 'alert-success');
        } catch (\Exception $e) {
            return view('message-view')
                ->with('message', 'The upload was successful')
                ->with('title', 'Inventory file upload failed: ' . $e->getMessage())
                ->with('class', 'alert-success');
        }

    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function viewInventory() {
        $inventoryData = InventoryModel::query()
            ->where('account_id', \auth()->user()->account_id)
            ->get();
        $columns = \Illuminate\Support\Facades\Schema::getColumnListing('inventory');
        return view('inventory.list')
            ->with('inventoryItems', $inventoryData)
            ->with('columns', $columns)
            ->with('title', 'Inventory List');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function editItem(Request $request, $id) {
        $title = 'Edit Item Details';
        try {
            $noEdit = array('id', 'account_id', 'created_at', 'updated_at', '_token', 'noEdit');
            $inventoryItemData = \Illuminate\Support\Facades\DB::table('inventory')
                ->where('id', $id)
                ->where('account_id', \auth()->user()->account_id)
                ->get();
            return view('inventory.edit-item')
                ->with('item', $inventoryItemData)
                ->with('title', $title)
                ->with('cnt', 0)
                ->with('noEdit', $noEdit);
        } catch(\Exception $e) {
            $message = 'There was a problem updating the Inventory Item, please try again later. ' . $e->getMessage();
            $class = 'alert-danger';
            return view('message-view')
                ->with('title', $title)
                ->with('message', $message)
                ->with('class', $class);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function processEditItem(Request $request) {
        try {
            $params = $request->all();
            $inventoryItem = new InventoryModel();
            $noEdit = json_decode($request->get('noEdit'));
            // I need to remove the id from the exception list so mySQL
            // can do an update instead of an insert
            unset($noEdit[array_search('id', $noEdit)]);
            foreach($params as $k => $v) {
                if(!in_array($k, $noEdit)) {
                    $inventoryItem->$k = $v;
                }
            }
            $title = 'Inventory Item Update';
            $inventoryItem->updated_at = date('Y-m-d H:i:s');
            DB::table('inventory')
                ->where('id', $inventoryItem->getAttribute('id'))
                ->where('account_id', \auth()->user()->account_id)
                ->update($inventoryItem->getAttributes());
            $message = 'The inventory item update was successful';
            $class = 'alert-success';
            return view('message-view')
                ->with('title', $title)
                ->with('message', $message)
                ->with('class', $class);
        } catch (\Exception $e) {
            $message = 'The inventory item update failed, please try again later. ' . $e->getMessage();
            $class = 'alert-danger';
            return view('message-view')
                ->with('title', $title)
                ->with('message', $message)
                ->with('class', $class);
        }
    }
}
