@extends('layouts.main-layout')

@section('content')
    <div class="alert {{$class}}" role="alert">{{$message}}</div>
@endsection
