@extends('layouts.main-layout')
<style type="text/css">
    td {
        white-space: nowrap;
        vertical-align: center !important;
        font-size: 12px !important;
    }

    th {
        white-space: nowrap;
        font-size: 12px !important;
        font-weight: bolder !important;
    }

    tr {
        height: 10px !important;
    }
</style>
@section('content')
    <div class="table-responsive">
        <table style="display: inline-table" class="table table-sm table-hover table-responsive">
            <thead>
            @foreach($columns as $column)
                <th>{{ucwords(str_replace('_', ' ', $column)) ?? '-'}}</th>
            @endforeach
            <th>Actions</th>
            </thead>
            @foreach($inventoryItems as $item)
                <tr>
                    <form action="/inventory/edit-item/{{$item->id}}" method="get">
                        @foreach($columns as $column)
                            <input type="hidden" name="{{$column}}" value="{{$item->$column ?? '-'}}">
                            <td>{{$item->$column ?? '-'}}</td>
                        @endforeach
                        <td class="actions">
                            <div class="dropdown">
                                <button class="btn btn-sm btn-primary dropdown-toggle listing-btn" type="button"
                                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    Select
                                </button>
                                <div class="dropdown-menu list-actions" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="/inventory/edit-item/{{$item->id}}"><i
                                            class="fas fa-edit"></i> Edit</a>
                                    <a class="dropdown-item" href="/order/add-item/{{$item->id}}"><i
                                            class="fas fa-plus"></i> Add to Order</a>
                                    <a class="dropdown-item" href="#"><i class="fas fa-trash-alt"></i> Delete</a>
                                </div>
                            </div>
                        </td>
                    </form>
                </tr>
            @endforeach
        </table>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var buttons = '<button type="button" class="btn btn-primary btn-sm">Add New</button>'
            $('.buttons').append(buttons);

            $('.order-cart').click(function () {
                window.location.href = "/order/show-orders";
            });
        });
    </script>
@endsection


