@extends('layouts.main-layout')
<style type="text/css">
    .center {
        margin: auto;
        width: 90%;
        padding: 10px;
    }
</style>
@section('content')
    <div class="center container">
        <form action="{{url('/inventory/process-edit-item')}}" method="post">
            @csrf
            @foreach($item as $key => $value)
                @foreach($value as $k => $v)
                    @if($cnt++ % 2 === 0)
                        <div class="row">
                            <div class="col-sm-6">
                                <label for="{{ucwords($k)}}">{{ucwords(str_replace('_', ' ', $k))}}</label>
                                <input name="{{strtolower($k)}}" type="text" class="form-control" id="{{ucwords($k)}}" value="{{$v ?? '-'}}"
                                    {{in_array($k, $noEdit) ? 'readonly' : ''}}>
                            </div>
                            @if($cnt === count((array)$value))
                        </div>
                            @endif
                        @else
                            <div class="col-sm-6">
                                <label for="{{ucwords($k)}}">{{ucwords(str_replace('_', ' ', $k))}}</label>
                                <input name="{{strtolower($k)}}" type="text" class="form-control" id="{{ucwords($k)}}" value="{{$v ?? '-'}}"
                                    {{in_array($k, $noEdit) ? 'readonly' : ''}}>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endforeach
            <input type="hidden" value="{{json_encode($noEdit)}}" name="noEdit">
            <br>
            <button type="submit" class="btn btn-primary">Submit</button>
            <br>
            <br>
            <br>
        </form>
    </div>
@endsection
