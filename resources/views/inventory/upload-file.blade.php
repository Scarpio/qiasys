@extends('layouts.main-layout')

@section('content')
    <form  style="margin-top: 25px;" class="col-lg-6 offset-lg-3 border" method="post" enctype="multipart/form-data" action="{{url('/inventory/load-data')}}">
        {{ csrf_field() }}
        <div class="form-group -pull-left">
            <label for="exampleFormControlFile1">Inventory data file in Excel format</label>
            <input type="file" name="excel_file" class="form-control-file">
        </div>
        <div class="-pull-right">
            <button style="margin-bottom: 5px;" type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection
