@extends('layouts.main-layout')
<style type="text/css">
    .center {
        margin: auto;
        width: 90%;
        padding: 10px;
    }
</style>
@section('content')
    <div class="center container">
        <form id="addToOrderFrm" action="{{url('/order/process-add-item')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Item Id</label>
                <input type="text" class="form-control" id="itemId" aria-describedby="itemId"
                       value="{{$inventoryItem->id}}" name="id" readonly>
            </div>
            <div class="form-group">
                <label for="itemDescription">Description</label>
                <input type="text" class="form-control" id="itemDescription" value="{{$inventoryItem->description}}"
                       name="description" readonly>
            </div>
            <div class="form-group">
                <label for="qtyAvailable">Quantity Available</label>
                <input type="text" class="form-control" id="qtyAvailable" value="{{$inventoryItem->quantity}}"
                       name="quantityAvailable" readonly>
                <br>
                <div class="alert alert-warning" role="alert">
                    The quantity available is subject to change until the transaction is finalized.
                </div>
            </div>
            <div class="form-group">
                <label for="qtyAvailable">Quantity to Order</label>
                <input type="text" class="form-control" id="qtyToOrder" name="quantityToOrder" value="">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>


    <script type="text/javascript" src="{{ URL::asset('js/jquery.validate.js') }}" defer></script>
    <script type="text/javascript" src="{{ URL::asset('js/additional-methods.js') }}" defer></script>

<script>
    $(document).ready(function() {
        $('#addToOrderFrm').validate({
            rules:{
                quantityToOrder: {
                    required: true,
                    number: true,
                    range: [1, $('#qtyAvailable').val()]
                }
            },
            messages: {
                quantityToOrder: {
                    required: "Quantity to Order is a required field.",
                    number: "Quantity to Order must be a number.",
                    range: "The Quantity to Order must between 1 and the Quantity Available.",
                }
            },
            submitHandler: function (validator) {
                validator.submit();
            }
        });
    });
</script>
@endsection
