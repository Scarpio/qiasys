@include('layouts.header')
@include('layouts.menu')

<div class="d-flex p-2">
    @yield('content')
</div>

@include('layouts.footer')




