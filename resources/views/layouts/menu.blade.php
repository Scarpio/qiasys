<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>


                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else


                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
{{--                            <a class="navbar-brand" href="#">Navbar</a>--}}
{{--                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--                                <span class="navbar-toggler-icon"></span>--}}
{{--                            </button>--}}

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }}
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
{{--                                <ul class="navbar-nav mr-auto">--}}
{{--                                    <li class="nav-item active">--}}
{{--                                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" href="#">Link</a>--}}
{{--                                    </li>                                    <li class="nav-item active">--}}
{{--                                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" href="#">Link</a>--}}
{{--                                    </li>--}}
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Inventory
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="/inventory/view"><i class="fas fa-eye"></i> View Inventory</a>
                                            <a class="dropdown-item" href="/inventory/upload-form"><i class="fas fa-cloud-upload-alt"></i> Upload Inventory</a>
{{--                                            <a class="dropdown-item" href="#">Another action</a>--}}
{{--                                            <div class="dropdown-divider"></div>--}}
{{--                                            <a class="dropdown-item" href="#">Something else here</a>--}}
                                        </div>
                                    </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Dispatch
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="/inventory/view"><i class="fas fa-plus"></i> Create Order</a>
{{--                                        <a class="dropdown-item" href="/inventory/upload-form"><i class="fas fa-cloud-upload-alt"></i> Upload Inventory</a>--}}
                                        {{--                                            <a class="dropdown-item" href="#">Another action</a>--}}
                                        {{--                                            <div class="dropdown-divider"></div>--}}
                                        {{--                                            <a class="dropdown-item" href="#">Something else here</a>--}}
                                    </div>
                                </li>
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link disabled" href="#">Disabled</a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                                <form class="form-inline my-2 my-lg-0">--}}
{{--                                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">--}}
{{--                                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--}}
{{--                                </form>--}}

                                <li class="nav-item nav-link navbar-light order-cart">
                                    <i class="fas fa-shopping-cart"></i> Order({{\App\Models\OrderCartModel::where('user_id', \auth()->user()->id)->count()}})
                                </li>
                            </div>
                        </nav>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    <br>
    <div class="container">
        <div class="row">
            <div class="col">
                <h3 class="title">{{$title ?? ''}}</h3>
            </div>
            <div class="col">
                <div class="buttons" style="text-align: right"></div>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.order-cart').click(function () {
            window.location.href = "/order/show-orders";
        });
    });
</script>
