<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

//Route::get('/dashboard', function () {
//    return view('layouts.main-layout');
//})->middleware(['auth'])->name('dashboard');

Route::get('/dashboard','App\Http\Controllers\DashboardController@index')->middleware('auth');

require __DIR__.'/auth.php';

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Inventory routes
Route::get('/inventory/upload-form', [App\Http\Controllers\InventoryController::class, 'displayUploadForm'])->middleware('auth');
Route::post('/inventory/load-data', [App\Http\Controllers\InventoryController::class, 'loadInventoryData'])->middleware('auth');
Route::get('/inventory/view', [App\Http\Controllers\InventoryController::class, 'viewInventory'])->middleware('auth');
Route::get('/inventory/edit-item/{id}', [App\Http\Controllers\InventoryController::class, 'editItem'])->middleware('auth');
Route::post('/inventory/process-edit-item', [App\Http\Controllers\InventoryController::class, 'processEditItem'])->middleware('auth');

// Order routes
Route::get('/order/add-item/{id}', [App\Http\Controllers\OrderController::class, 'addToOrderForm'])->middleware('auth');
Route::post('/order/process-add-item', [App\Http\Controllers\OrderController::class, 'processAddToOrderForm'])->middleware('auth');
Route::get('/order/show-orders', [App\Http\Controllers\OrderController::class, 'showOrders'])->middleware('auth');
