<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNullablesToInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory', function (Blueprint $table) {
            $table->string('consignee')->nullable()->change();
            $table->string('shipper')->nullable()->change();
            $table->string('purchase_order')->nullable()->change();
            $table->string('carrier')->nullable()->change();
            $table->string('airway_bill')->nullable()->change();
            //$table->integer('quantity')->nullable()->change();
            $table->time('eta_time')->nullable()->change();
            $table->time('arrival_time')->nullable()->change();
            $table->string('broker')->nullable()->change();
            $table->string('driver')->nullable()->change();
            $table->string('origin')->nullable()->change();
            //$table->string('description')->nullable()->change();
            $table->integer('net_weight_lbs')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory', function (Blueprint $table) {
            //
        });
    }
}
