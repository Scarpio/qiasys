<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('consignee');
            $table->string('shipper');
            $table->string('purchase_order');
            $table->string('carrier');
            $table->string('airway_bill');
            $table->integer('quantity');
            $table->time('eta_time');
            $table->time('arrival_time');
            $table->string('broker');
            $table->string('driver');
            $table->string('origin');
            $table->string('description');
            $table->integer('net_weight_lbs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory');
    }
}
